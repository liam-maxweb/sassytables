<div class="table-container">
    <div class="table-responsive">
        <table class="table sassytable">
            <thead>
                <tr>
                    @foreach ($table->columns as $column)
                        <th class="{{ $column['class'] ?? '' }}">
                            @if (! empty($column['sortable']) && $column['sortable'] === true)
                                <a href="{{ $table->url->sort($column['column']) }}">
                                    {{ $column['name'] }}

                                    @if ($table->sort === $column['column'])
                                        @if ($table->order === 'asc')
                                            @icon (['icon' => 'fa-sort-up'])
                                            @endicon
                                        @else
                                            @icon (['icon' => 'fa-sort-down'])
                                            @endicon
                                        @endif
                                    @endif
                                </a>
                            @else
                                <a href="#">
                                    {{ $column['name'] }}
                                </a>
                            @endif
                        </th>
                    @endforeach

                    @if ($table->canEdit)
                        <th></th>
                    @endif

                    @if ($table->canDelete)
                        <th></th>
                    @endif
                </tr>
            </thead>

            @if (count($table->object) > 0)
                <tbody>
                    @foreach ($table->object as $obj)
                        <tr>
                            @foreach ($table->columns as $column)
                                <td class="{{ $column['class'] ?? '' }}">
                                    @if (! empty($column['links']))
                                        <a href="{{ $obj->url->view }}">
                                            @if (! empty($column['links'][4]))
                                                {!! ((! empty($column['prefix'])) ? $column['prefix'] : '') . optional(optional(optional(optional(optional($obj->{$column['links'][0]})->{$column['links'][1]})->{$column['links'][2]})->{$column['links'][3]})->{$column['links'][4]})->{$column['column']} !!}
                                            @elseif (! empty($column['links'][3]))
                                                {!! ((! empty($column['prefix'])) ? $column['prefix'] : '') . optional(optional(optional(optional($obj->{$column['links'][0]})->{$column['links'][1]})->{$column['links'][2]})->{$column['links'][3]})->{$column['column']} !!}
                                            @elseif (! empty($column['links'][2]))
                                                {!! ((! empty($column['prefix'])) ? $column['prefix'] : '') . optional(optional(optional($obj->{$column['links'][0]})->{$column['links'][1]})->{$column['links'][2]})->{$column['column']} !!}
                                            @elseif (! empty($column['links'][1]))
                                                {!! ((! empty($column['prefix'])) ? $column['prefix'] : '') . optional(optional($obj->{$column['links'][0]})->{$column['links'][1]})->{$column['column']} !!}
                                            @elseif (! empty($column['links'][0]))
                                                {!! ((! empty($column['prefix'])) ? $column['prefix'] : '') . optional($obj->{$column['links'][0]})->{$column['column']} !!}
                                            @endif
                                        </a>
                                    @elseif (! empty($column['column']))
                                        @php
                                            $params = [];

                                            if (is_array($table->rowRouteParams) && count($table->rowRouteParams) > 0) {
                                                foreach ($table->rowRouteParams as $param) {
                                                    $params[] = $obj->{$param};
                                                }
                                            }
                                        @endphp

                                        <a href="{{ $obj->url->view ?? '' }}">
                                            @if (! empty($column['boolean']) && $column['boolean'] === true)
                                                @if ($obj->{$column['column']} === true || $obj->{$column['column']} === 1)
                                                    @icon (['icon' => 'fa-check'])
                                                    @endicon
                                                @else
                                                    @icon (['icon' => 'fa-times'])
                                                    @endicon
                                                @endif
                                            @else
                                                {!! ((! empty($column['prefix'])) ? $column['prefix'] : '') . $obj->{$column['column']} !!}
                                            @endif
                                        </a>
                                    @endif
                                </td>
                            @endforeach

                            @if ($table->canEdit)
                                <td>
                                    <a href="{{ $obj->url->edit }}" class="table-edit">
                                        @icon (['icon' => 'fa-edit'])
                                        @endicon
                                    </a>
                                </td>
                            @endif

                            @if ($table->canDelete)
                                <td>
                                    <a href="{{ $obj->url->delete }}" class="table-delete" data-delete-prompt>
                                        @icon (['icon' => 'fa-trash-alt'])
                                        @endicon
                                    </a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            @else
                <tbody>
                    <tr>
                        <td colspan="5">
                            <a>{{ $table->noDataString }}</a>
                        </td>
                    </tr>
                </tbody>
            @endif

            @if ($table->footer)
                <tfoot>
                    <tr>
                        @foreach ($table->columns as $column)
                            <td>
                                @if (! empty($column['footer']))
                                    @if (! empty($column['footer']['callback']))
                                        {!! ((! empty($column['prefix'])) ? $column['prefix'] : '') . call_user_func_array($column['footer']['callback']['method'], $column['footer']['callback']['arguments']) !!}
                                    @else
                                        {!! ((! empty($column['prefix'])) ? $column['prefix'] : '') . $column['footer'] !!}
                                    @endif
                                @endif
                            </td>
                        @endforeach
                    </tr>
                </tfoot>
            @endif
        </table>
    </div>
</div>

@if ($table->paginated)
    {{ $table->object->appends($table->appends)->links() }}
@endif